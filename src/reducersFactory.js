import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import homePageReducer from './homePage/HomePageReducer';

export default combineReducers({
  routing,
  homePageReducer,
});
