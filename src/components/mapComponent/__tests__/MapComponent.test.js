import React from 'react';
import ReactDOM from 'react-dom';
import MapComponent from '../MapComponent';

it('renders without crashing', () => {
  const div = document.createElement('div');

  ReactDOM.render(
    <MapComponent
      googleMapURL="https://maps.googleapis.com/maps/api/js?&v=3.exp&libraries=geometry,drawing,places"
      loadingElement={ <div className='map-loading-element' /> }
      containerElement={ <div className='map-container-element' /> }
      mapElement={ <div className='map' /> }
      isMarkerShown
      markers={ [] }
      center={ {} }
      onClickMarker={ jest.fn() }
    />
    , div);
});
