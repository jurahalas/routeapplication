import React, { Component } from 'react';
import { Marker } from 'react-google-maps';
import markerIcon from '../../images/location-blue.png';
import PropTypes from 'prop-types';
import './MapComponent.css';

class MarkerComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      center: { lat: -34.397, lng: 150.644 },
    };
  }

  handleClick = () => {

  };

  render() {
    const { position } = this.props;

    return (
      <Marker
        onClick={ this.handleClick }
        position={ position }
        defaultIcon={ markerIcon }
      />
    );
  }
}

MarkerComponent.propTypes = {
  position: PropTypes.object.isRequired,
  onClickMarker: PropTypes.func,
};

export default MarkerComponent;