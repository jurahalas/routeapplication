import React, { Component } from 'react';
import MapComponent from '../../components/mapComponent/MapComponent';
import PropTypes from 'prop-types';
import { GOOGLE_MAP_API_KEY } from '../mapComponent/Constants';

class MapContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPosition: { lat: 222.222, lng: 222.222 },
      _mounted: false,
    };
  }

  componentDidMount() {
    this.setState({
      _mounted: true,
    });

    if(navigator.geolocation && this.state._mounted)
      navigator.geolocation.getCurrentPosition((position) => {
        this.setState({
          currentPosition: {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          },
        });
      });
  }

  componentWillUnmount () {
    this.setState({
      _mounted: false,
    });
  }

  render() {
    const { width, height, center, markerTo, destination } = this.props;

    return (
      <div className='map-wrapper' style={ { width, height } }>
        <MapComponent
          googleMapURL={ `https://maps.googleapis.com/maps/api/js?key=${GOOGLE_MAP_API_KEY}&v=3.exp&libraries=geometry,drawing,places` }
          loadingElement={ <div className='map-loading-element' /> }
          containerElement={ <div className='map-container-element' /> }
          mapElement={ <div className='map' /> }
          markerTo={ markerTo }
          center={ center || this.state.currentPosition }
          destination={ destination }
        />
      </div>
    );
  }
}

MapContainer.propTypes = {
  height: PropTypes.string.isRequired,
  width: PropTypes.string.isRequired,
  destination: PropTypes.string.isRequired,
  center: PropTypes.object,
  markerTo: PropTypes.object,
};

export default MapContainer;