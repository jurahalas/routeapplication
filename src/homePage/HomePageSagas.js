import { put, call, takeLatest, all } from 'redux-saga/effects';
import { processRequest } from '../services/Api';
import { homePageActionTypes } from './HomePageConstants';
import * as homePageActions from './HomePageActions';
import { geocodeByAddress, getLatLng } from 'react-places-autocomplete';

export default function* () {
  yield all([
    yield takeLatest(homePageActionTypes.GET_HOME_PAGE_DATA_REQUEST, handleGetHomePageDataRequest),
    yield takeLatest(homePageActionTypes.GET_GEOCODE_BY_ADDRESS, handleGetGeocodeByAddress),
  ]);
}

export function* handleGetHomePageDataRequest() {
  try {
    const data = yield call(processRequest, 'getData');

    yield put(homePageActions.getHomePageDataSuccess(data));
  } catch(e) {
    yield put(homePageActions.getHomePageDataError(e));
  }
}

export function* handleGetGeocodeByAddress(action) {
  try {
    const { address } = action.payload;
    const geocode = yield call(geocodeByAddress, address);
    const latLng = yield call(getLatLng, geocode[0]);

    yield put(homePageActions.getGeocodeByAddressSuccess(latLng));
  } catch(e) {
    yield put(homePageActions.getGeocodeByAddressError(e));
  }
}
