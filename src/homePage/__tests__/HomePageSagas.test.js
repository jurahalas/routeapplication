import * as sagas from '../HomePageSagas';
import { put, call } from 'redux-saga/effects';
import * as homePageActions from '../HomePageActions';
import { geocodeByAddress, getLatLng } from 'react-places-autocomplete';

describe('Get geocode by address tests', () => {
  const action = {
    payload: {
      address: 'string',
    },
  };
  const { address } = action.payload;

  it('Should successfully get geocode by address', () => {
    const generator = sagas.handleGetGeocodeByAddress(action);

    let next = generator.next();

    expect(next.value).toEqual(call(geocodeByAddress, address));

    next = generator.next(call(geocodeByAddress, address));
    expect(next.value).toEqual(call(getLatLng, call(geocodeByAddress, address)[0]));

    next = generator.next(call(getLatLng, call(geocodeByAddress, address)[0]));
    expect(next.value).toEqual(put(homePageActions.getGeocodeByAddressSuccess(call(getLatLng, call(geocodeByAddress, address)[0]))));

    next = generator.next();
    expect(next.done).toEqual(true);
  });

  it('Should failed on error', () => {
    const generator = sagas.handleGetGeocodeByAddress(action);

    let next = generator.next();

    expect(next.value).toEqual(call(geocodeByAddress, address));

    next = generator.next(call(geocodeByAddress, address));
    expect(next.value).toEqual(call(getLatLng, call(geocodeByAddress, address)[0]));

    next = generator.next();
    expect(next.value).toEqual(put(homePageActions.getGeocodeByAddressError(new Error('500 Internal Server Error'))));

    next = generator.next();
    expect(next.done).toEqual(true);
  });
});
