import keyMirror from 'keymirror';

export const homePageActionTypes = keyMirror(
  {
    GET_HOME_PAGE_DATA_REQUEST: null,
    GET_HOME_PAGE_DATA_SUCCESS: null,
    GET_HOME_PAGE_DATA_ERROR: null,
    OPEN_CREATE_ROUTE_MODAL: null,
    CLOSE_CREATE_ROUTE_MODAL: null,
    GET_GEOCODE_BY_ADDRESS: null,
    GET_GEOCODE_BY_ADDRESS_SUCCESS: null,
    GET_GEOCODE_BY_ADDRESS_ERROR: null,
    CLEAN_ADDRESS: null,
  }
);
