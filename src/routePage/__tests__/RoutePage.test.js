import React from 'react';
import RoutePage from '../RoutePage';
import { shallow } from 'enzyme';

it('renders without crashing', () => {
  const div = document.createElement('div');

  shallow(
    <RoutePage />,
    div);
});
