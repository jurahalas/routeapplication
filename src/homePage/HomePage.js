import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as homePageActions from './HomePageActions';
import CreateRouteModal from '../components/modals/createRouteModal/CreateRouteModal';
import './HomePage.css';

class HomePage extends PureComponent {
  componentDidMount() {
    this.props.homePageActions.getHomePageDataRequest();
  }

  handleSelect = (address) => {
    this.props.homePageActions.getGeoCodeByAddress(address);
  };
  submitClickHandler =() => {
    const { history, homePageReducer: { address } } = this.props;

    history.push(`/map/${ address }`);
  };

  render() {
    const { homePageReducer, homePageActions: { openCreateRouteModal, closeCreateRouteModal } } = this.props;
    const { data, createRouteModal: { open }, address } = homePageReducer;
    const { title, description } = data;

    return (
      <div className='home-page-container'>
        <div className='home-page-title'>
          { title }
        </div>
        <div className='home-page-description'>
          { description }
        </div>
        <button
          className="home-page-btn"
          onClick={ openCreateRouteModal }
        >
          Create route
        </button>
        <CreateRouteModal
          isOpen={ open }
          width={ 600 }
          height={ 470 }
          cancelClickHandler={ closeCreateRouteModal }
          submitClickHandler={ this.submitClickHandler }
          handleSelect={ this.handleSelect }
          address={ address }
        />
      </div>
    );
  }
}

HomePage.propTypes = {
  homePageActions: PropTypes.object,
  homePageReducer: PropTypes.object,
  history: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    homePageReducer: state.homePageReducer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    homePageActions: bindActionCreators(homePageActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);