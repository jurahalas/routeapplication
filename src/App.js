import React, { Component } from 'react';
import './App.css';
import PropTypes from 'prop-types';

class App extends Component {
  render() {
    return (
      <div className="template-wrapper" >
        { this.props.children }
      </div>
    );
  }
}

App.propTypes = {
  children: PropTypes.object,
};

export default App;
