import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withScriptjs, withGoogleMap, GoogleMap, Polyline, Marker } from 'react-google-maps';
import markerIcon from '../../images/location-blue.png';
import { MinskLatLng } from '../../services/Constants';
import MarkerComponent from '../../components/mapComponent/MarkerComponent';

class MapComponent extends Component {
  constructor() {
    super();
    this.state = {
      coords: {},
    };
  }

  componentDidMount() {
    const google = window.google;
    const DirectionsService = new google.maps.DirectionsService();

    DirectionsService.route({
      origin: 'Minsk',
      destination: this.props.destination,
      travelMode: google.maps.TravelMode.WALKING,
      provideRouteAlternatives: true,
      optimizeWaypoints: true,
    }, (result, status) => {
      if(status === google.maps.DirectionsStatus.OK) {
        const coords = result.routes[0].overview_path;

        this.setState({
          coords: coords,
        });
      } else 
        console.error(`error fetching directions ${result}`);
    });
  }

  render() {
    const { markerTo } = this.props;

    return (
      <div>
        <GoogleMap
          defaultZoom={ 4 }
          defaultCenter={ MinskLatLng }
          center={ MinskLatLng }
        >
          {this.state.coords && (
            <Polyline
              path={ this.state.coords }
              geodesic={true}
              options={{
                strokeColor: '#ff2343',
                strokeOpacity: 0.8,
                strokeWeight: 5,
                clickable: true,
              }}
            />
          )}
          <Marker
            position={new window.google.maps.LatLng(MinskLatLng)}
          />
          <Marker
            defaultIcon={ markerIcon }
            position={new window.google.maps.LatLng(markerTo)}
          />
        </GoogleMap>
      </div>
    );
  }
}

MapComponent.propTypes = {
  markerTo: PropTypes.array,
  destination: PropTypes.string,
};

export default withScriptjs(withGoogleMap(MapComponent));