import React, { Component, Fragment } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import './CreateRouteModal.css';
import PlacesAutocomplete from 'react-places-autocomplete';

class CreateRouteModal extends Component {
  constructor(props) {
    super(props);
    this.state = { address: '' };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      address: nextProps.address,
    });
  }

  handleChange = address => {
    this.setState({ address });
  };

  autocompleteItem = ({ formattedSuggestion }) => (
    <div className="suggestion-item">
      { ' ' }<strong>{ formattedSuggestion.mainText }</strong>{ ' ' }
      <small>{ formattedSuggestion.secondaryText }</small>
    </div>
  );

  onError = (status, clearSuggestions) => {
    clearSuggestions();
  };

  render() {
    const {
      isOpen,
      height,
      width,
      cancelClickHandler,
      submitClickHandler,
      loading,
      handleSelect,
    } = this.props;

    const style = {
      width,
      height,
      top: `calc(50% - ${height / 2}px)`,
      left: `calc(50% - ${width / 2}px)`,
    };

    return (
      (isOpen || loading) &&
      ReactDOM.createPortal(
        <Fragment>
          <div className="create-route-modal-shadow" />
          <div style={ style } className='create-route-modal-main'>
            <div className='create-route-modal-main-wrapper'>

              <div className='create-route-modal-content'>
                { (isOpen && !loading) &&
                <Fragment>
                  <div className="create-route-modal-title">Create new route</div>
                  <PlacesAutocomplete
                    onSelect={ handleSelect }
                    renderSuggestion={ this.autocompleteItem }
                    onError={ this.onError }
                    inputProps={ {
                      onChange: this.handleChange,
                      value: this.state.address,
                      placeholder: 'Enter your address',
                    } }
                    classNames={ {
                      input: 'form-control',
                      autocompleteContainer: 'autocomplete-container',
                    } }
                  />
                </Fragment>
                }
              </div>
              <div className='create-route-modal-bottom'>
                <div className='create-route-modal-bottom-buttons'>
                  <button
                    className='create-route-modal-bottom-cancel'
                    onClick={ cancelClickHandler }
                  >
                    Cancel
                  </button>
                  <button
                    className='create-route-modal-bottom-submit'
                    onClick={ submitClickHandler }
                  >
                    Create
                  </button>
                </div>
              </div>
            </div>
          </div>
        </Fragment>,
        document.querySelector('body')
      )
    );
  }
}

CreateRouteModal.defaultProps = {
  isOpen: false,
  height: 300,
  width: 300,
  loading: false,
};

CreateRouteModal.propTypes = {
  isOpen: PropTypes.bool,
  loading: PropTypes.bool,
  height: PropTypes.any,
  width: PropTypes.any,
  cancelClickHandler: PropTypes.func,
  submitClickHandler: PropTypes.func,
  address: PropTypes.string,
  handleSelect: PropTypes.func,
};

export default CreateRouteModal;
