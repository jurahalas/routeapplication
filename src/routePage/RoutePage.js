import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as homePageActions from '../homePage/HomePageActions';
import './RoutePage.css';
import MapContainer from '../components/mapComponent/MapContainer';

class RoutePage extends PureComponent {
  componentDidMount() {
    const { match: { params }, homePageActions } = this.props;

    homePageActions.getGeoCodeByAddress(params.address);
  }

  componentWillUnmount() {
    this.props.homePageActions.cleanAddress();
  }

  render() {
    const { homePageReducer, match } = this.props;
    const { data, address, latLng } = homePageReducer;
    const { title } = data;
    const { params } = match;

    return (
      <div className='route-page-container'>
        <div className='route-page-title'>
          { title }
        </div>
        <button className="route-page-btn" onClick={ this.props.history.goBack }>Set new route</button>
        <div className="route-page-map">
          <MapContainer
            height='100%'
            width='100%'
            markerTo={ latLng }
            destination={ address || params.address }
          />
        </div>
      </div>
    );
  }
}

RoutePage.propTypes = {
  homePageReducer: PropTypes.object,
  homePageActions: PropTypes.object,
  match: PropTypes.object,
  history: PropTypes.object,

};

function mapStateToProps(state) {
  return {
    homePageReducer: state.homePageReducer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    homePageActions: bindActionCreators(homePageActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(RoutePage);