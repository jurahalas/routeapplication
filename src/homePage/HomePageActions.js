import { homePageActionTypes } from './HomePageConstants';

export function getHomePageDataRequest() {
  return {
    type: homePageActionTypes.GET_HOME_PAGE_DATA_REQUEST,
  };
}

export function getHomePageDataSuccess(data) {
  return {
    type: homePageActionTypes.GET_HOME_PAGE_DATA_SUCCESS,
    payload: {
      data,
    },
  };
}

export function getHomePageDataError(error) {
  return {
    type: homePageActionTypes.GET_HOME_PAGE_DATA_ERROR,
    payload: {
      error,
    },
  };
}

export function openCreateRouteModal() {
  return {
    type: homePageActionTypes.OPEN_CREATE_ROUTE_MODAL,
  };
}

export function closeCreateRouteModal() {
  return {
    type: homePageActionTypes.CLOSE_CREATE_ROUTE_MODAL,
  };
}

export function getGeoCodeByAddress(address) {
  return {
    type: homePageActionTypes.GET_GEOCODE_BY_ADDRESS,
    payload: {
      address,
    },
  };
}

export function getGeocodeByAddressSuccess(latLng) {
  return {
    type: homePageActionTypes.GET_GEOCODE_BY_ADDRESS_SUCCESS,
    payload: {
      latLng,
    },
  };
}

export function getGeocodeByAddressError(errors) {
  return {
    type: homePageActionTypes.GET_GEOCODE_BY_ADDRESS_ERROR,
    payload: {
      errors,
    },
  };
}
export function cleanAddress() {
  return {
    type: homePageActionTypes.CLEAN_ADDRESS,
  };
}
