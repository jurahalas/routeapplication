import { homePageActionTypes } from './HomePageConstants';

const initialState = {
  loading: false,
  data: {
    title: 'Route application',
    description: ' Application descriptions. To create new route please click on the button "Create".',
  },
  createRouteModal: {
    open: false,
  },
  address: '',
  latLng: {
    lat: null,
    lng: null,
  },
};

export default function homePageReducer(state = initialState, action) {
  const { type, payload } = action;

  switch(type) {
    case homePageActionTypes.GET_HOME_PAGE_DATA_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case homePageActionTypes.GET_HOME_PAGE_DATA_SUCCESS:
      const { title, description } = payload.data;

      return {
        ...state,
        loading: false,
        title,
        description,
      };

    case homePageActionTypes.GET_HOME_PAGE_DATA_ERROR:
      return {
        ...state,
        loading: false,
      };

    case homePageActionTypes.OPEN_CREATE_ROUTE_MODAL:
      return {
        ...state,
        createRouteModal: {
          open: true,
        },
      };

    case homePageActionTypes.CLOSE_CREATE_ROUTE_MODAL:
      return {
        ...state,
        createRouteModal: {
          open: false,
        },
        address: '',
      };

    case homePageActionTypes.GET_GEOCODE_BY_ADDRESS:
      const { address } = payload;

      return {
        ...state,
        address,
      };

    case homePageActionTypes.GET_GEOCODE_BY_ADDRESS_SUCCESS:
      return {
        ...state,
        latLng: payload.latLng,
      };

    case homePageActionTypes.CLEAN_ADDRESS:
      return {
        ...state,
        address: '',
      };

    default:
      return state;
  }
}