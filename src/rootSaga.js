import { fork } from 'redux-saga/effects';
import homePageSagas from './homePage/HomePageSagas';

export default function* rootSaga() {
  yield fork(homePageSagas);
}