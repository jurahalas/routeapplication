import React from 'react';
import ReactDOM from 'react-dom';
import MapContainer from '../MapContainer';

it('renders without crashing', () => {
  const div = document.createElement('div');

  ReactDOM.render(
    <MapContainer
      height='400px'
      width='400px'
      center={ {} }
    />, div);
});
