import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Switch, Route } from 'react-router';
import { ConnectedRouter } from 'react-router-redux';
import './index.css';
import Store from './store/Store';
import App from './App';
import { appHistory } from './services/HistoryConfig';
import PageNotFound from './pageNotFound/PageNotFound';
import HomePage from './homePage/HomePage';
import RoutePage from './routePage/RoutePage';

class Main extends Component {
  renderHomePage = (props) => (
    <HomePage { ...props } route={ { name: 'HomePage', parent: 'HomePage' } } />
  );

   renderRoutePage = (props) => (
     <RoutePage { ...props } route={ { name: 'RoutePage', parent: 'RoutePage', search: props.history.location.search } } />
   );

   render() {
     return (
       <Provider store={ Store }>
         <ConnectedRouter history={ appHistory }>
           <App>
             <Switch>
               <Route
                 exact={true}
                 path='/'
                 render={ this.renderHomePage }
               />
               <Route
                 exact={true}
                 path='/map/:address'
                 render={ this.renderRoutePage }
               />
               <Route name="NotFound" component={ PageNotFound } />
             </Switch>
           </App>
         </ConnectedRouter>
       </Provider>
     );
   }
}

ReactDOM.render(
  <Main />,
  document.getElementById('root')
);
