import React from 'react';
import { shallow } from 'enzyme';
import HomePage from '../HomePage';
import { Provider } from 'react-redux';
import { configureStore } from '../../store/Store';

const store = configureStore();

it('renders without crashing', () => {
  shallow(
    <Provider store={ store }>
      <HomePage
        handleSubmit={ jest.fn() }
        homePageActions={ {} }
        homePageReducer={ {} }
      />
    </Provider>);
});